/**
 * Created by septimus on 06/05/17.
 */

(function()
{
    angular.module('AppNeuralNet').controller('GeneticGameController', GeneticGameController);

    GeneticGameController.$inject = ['$scope', 'NeuralNet'];

    function GeneticGameController($scope, NeuralNet)
    {
        class Sensor
        {
            constructor(maxRange, relativeAngle)
            {
                this.maxRange = maxRange;
                this.relativeAngle = relativeAngle;

                this.x = 0;
                this.y = 0;
                this.alphaColor = 0;
                this.currentRange = this.maxRange;
            }

            getCurrentRange()
            {
                return this.currentRange;
            }
            update(startX, startY, creatureAngle, foods)
            {
                const filteredFoods = this.filterFoods(startX, startY, foods);
                let ranges = [];
                for(let i = 0; i < filteredFoods.length; ++i)
                {
                    const intersections = findIntersections(filteredFoods[i], { x: startX, y: startY }, this);
                    for(let j = 0; j < intersections.length; ++j)
                        ranges.push(Math.hypot(startX - intersections[j].x, startY - intersections[j].y));
                }

                this.currentRange = Math.min(...ranges);

                if(this.currentRange > this.maxRange)
                    this.currentRange = this.maxRange;

                this.alphaColor = 1 - (this.currentRange / this.maxRange) + .01;

                this.x = rotatedLineX(startX, creatureAngle + this.relativeAngle, this.currentRange);
                this.y = rotatedLineY(startY, creatureAngle + this.relativeAngle, this.currentRange);
            }

            filterFoods(startX, startY, foods)
            {
                let result = [];
                const totalRadius = ($scope.configs.foods.size + this.maxRange) / 2;
                const sensorCenter = this.getSensorCenter(startX, startY);

                for(let i = 0; i < foods.length; ++i)
                {
                    if(totalRadius > Math.hypot(sensorCenter.x - foods[i].x, sensorCenter.y - foods[i].y))
                        result.push(foods[i]);
                }
                return result;
            }

            getSensorCenter(x, y)
            {
                let result = {};
                result.x = (x + this.x) / 2;
                result.y = (y + this.y) / 2;
                return result;
            }
        }
        class Creature
        {
            constructor(x, y, startAngle, sensorsConfig, name)
            {
                this.name = name;

                this.x = x;
                this.y = y;
                this.angle = startAngle;
                this.acceleration = .1;
                this.velocity = 0;
                this.vx = 0;
                this.vy = 0;

                this.life = 500;
                this.score = 0;

                this.sensorsConfig = sensorsConfig;
                this.sensors = [];
                this.ai = NeuralNet.createNewNeuralNet($scope.configs.neuralNet);
                this.constructSensors();
            }

            constructSensors()
            {
                const fov = this.sensorsConfig.fieldOfView;
                const sensorsCount = this.sensorsConfig.sensorsCount;
                const fovStep = fov / (sensorsCount - 1);

                let step = -fov / 2;
                let i = 0;
                for(step, i; i < sensorsCount; ++i, step += fovStep)
                {
                    let sensor = new Sensor(this.sensorsConfig.sensorsRange, step);
                    sensor.update(this.x, this.y, this.angle, []);
                    this.sensors.push(sensor);
                }
            }

            tryToEat()
            {
                let l = $scope.foods.length;
                let mouthRadius = 5;
                let toRemove = [];
                for(let i = 0; i < l; ++i)
                {
                    let totalRadius = mouthRadius + $scope.foods[i].size;
                    if(totalRadius > Math.hypot(this.x - $scope.foods[i].x, this.y - $scope.foods[i].y))
                    {
                        this.life += $scope.foods[i].energy;
                        $scope.foods.splice(i, 1);
                        --i;
                    }
                }
            }

            update()
            {
                this.life--;
                this.tryToEat();

                if(this.life <= 0)
                    return;
                this.score++;
                let inputs = [];
                for(let i = 0; i < this.sensors.length; ++i)
                {
                    this.sensors[i].update(this.x, this.y, this.angle, $scope.foods);
                    inputs.push(this.sensors[i].getCurrentRange());
                }
                inputs.push(this.velocity / 2);
                inputs.push(this.acceleration * 10);

                this.ai.setInputs(inputs);
                this.ai.activate();
                let outputs = this.ai.getOutputs();

                if(outputs[0] > outputs[1])
                    this.acceleration = .1;
                else
                    this.acceleration = -.1;

                if(outputs[2] > .5 && outputs[2] > outputs[3])
                    this.angle++;
                else if(outputs[3] > .5 && outputs[3] > outputs[2])
                    this.angle--;

                this.updatePosition()
            }

            updatePosition()
            {
                let x = this.x;
                let y = this.y;

                this.velocity += this.acceleration;
                if(this.velocity > 2)
                    this.velocity = 2;
                if(this.velocity < 0)
                    this.velocity = 0;
                this.vx = this.velocity * Math.sin(toRadians(this.angle));
                this.vy = -this.velocity * Math.cos(toRadians(this.angle));

                this.x += this.vx;
                this.y += this.vy;
                if(this.x > 500)
                    this.x = 500;
                if(this.y > 250)
                    this.y = 250;
                if(this.x <0)
                    this.x = 0;
                if(this.y < 0)
                    this.y = 0;

                let d = Math.hypot(x - this.x, y - this.y);
                //this.life += d;
            }

        }

        $scope.configs = { creatures: {}, neuralNet: {}, foods: {} };
        $scope.configs.creatures =
        {
            sensorsCount: 7,
            sensorsRange: 150,
            fieldOfView: 135 // in degree
        };
        $scope.configs.neuralNet =
        [
            {
                neuronsCount: $scope.configs.creatures.sensorsCount + 2, // 2: speed and acceleration state
                function: 'linear'
            },
            {
                neuronsCount: $scope.configs.creatures.sensorsCount * 3 + 2 + 3, // 3 neurons per sensors + 2 for speed + 3 for acceleration state
                function: 'sigmoid'
            },
            {
                neuronsCount: $scope.configs.creatures.sensorsCount * 2 + 1 + 1, // 2 neurons per sensors + 1 for speed + 1 for acceleration state
                function: 'linear'
            },
            {
                neuronsCount: 4, // outputs: speed up/down ; direction left/right
                function: 'linear'
            }
        ];
        $scope.configs.foods =
        {
            size: 10
        };

        $scope.creatures = [];
        $scope.foods = [];

        function addFood()
        {
            for(let i = 0; i < $scope.stillAlive / 2; ++i)
            {
                const x = random(0, 500);
                const y = random(0, 250);
                const size = 3;
                $scope.foods.push({size: size, x: x, y: y, energy: 25});
            }
        }

        function resetFoods()
        {
            $scope.foods = [];
            for(let i = 0; i < 5; ++i)
                addFood();
        }

        $scope.totalCreature = 0;
        $scope.generation = 0;
        $scope.stillAlive = 30;
        $scope.dontEnd = false;
        $scope.bestPerGeneration = [];
        $scope.mergeHistory = [];
        $scope.selectedDNA = {};
        $scope.display = true;

        function createCreature(dna, n)
        {
            const x = random(50, 450); // 200
            const y = random(50, 200); // 100
            const startAngle = random(0, 360);
            let name = $scope.generation + ':';
            if(n !== undefined)
                name += n + ':';
            name += (++$scope.totalCreature);
            let c = new Creature(x, y, startAngle, $scope.configs.creatures, name);

            if(dna !== undefined)
                c.ai.implantDNA(dna);
            $scope.creatures.push(c);
            console.log("createCreature: " + $scope.creatures[$scope.creatures.length - 1].name);
        }

        function fillGeneration()
        {
            for(let i = $scope.creatures.length; i < 30; ++i)
                createCreature();
        }

        function checkEndGeneration()
        {
            if($scope.dontEnd)
                return false;
            if($scope.stillAlive < 6)
                return true;    // endFromDeath
            $scope.creatures = $scope.creatures.sort(function(a, b) { return b.score - a.score; });
            return $scope.creatures[4].score > 2000; // endFromScore
        }

        function updateGame()
        {
            if($scope.dontEnd)
                return;
            $scope.stillAlive = 0;
            let l = $scope.creatures.length;
            for(let i = 0; i < l; i++)
            {
                $scope.creatures[i].update();
                if($scope.creatures[i].life > 0)
                    ++$scope.stillAlive;
            }

            if(checkEndGeneration())
                $scope.endGeneration();
            else
            {
                $scope.creatures = $scope.creatures.sort(function(a, b) { return b.life - a.life; });
                $scope.selectedDNA.genes = transformForDisplay($scope.creatures[0].ai.extractDNA().genes);
                $scope.selectedDNA.name = $scope.creatures[0].name;
                $scope.$apply();
            }
        }

        function transformForDisplay(dna)
        {
            let tmp = [];
            let result = [];
            let min = Math.min(...dna);
            let max = Math.max(Math.max(...dna), Math.abs(min)) * 2;
            let lineLength = Math.sqrt(dna.length / 3) | 0;

            for(let i = 0; i < dna.length; i+=3)
                tmp.push({
                    r: ((dna[i] - min) / max * 255) | 0,
                    g: ((dna[i + 1] - min) / max * 255) | 0,
                    b: ((dna[i + 2] - min) / max * 255) | 0});
            for(let i = 0; i < tmp.length; i += lineLength)
                result.push(tmp.slice(i, i + lineLength));
            return result;
        }

        function mergeCreature(first, second)
        {
            let dna1 = $scope.creatures[first].ai.extractDNA();
            let dna2 = $scope.creatures[second].ai.extractDNA();
            let name = $scope.creatures[second].name.split(':')[1] + ':' + $scope.creatures[second].name.split(':')[1];

            dna1.merge($scope.creatures[second].ai.extractDNA(), .2);
            dna2.merge($scope.creatures[first].ai.extractDNA(), .2);
            createCreature(dna1, name);
            createCreature(dna2, name);

            $scope.mergeHistory.push({id: $scope.creatures[first].name, from: $scope.creatures[second].name});
        }

        function resetGeneration()
        {
            let l = $scope.creatures.length;
            console.log(l);


            $scope.bestPerGeneration.push({name: $scope.creatures[0].name, score: $scope.creatures[0].score, life: $scope.creatures[0].life, generation: $scope.generation - 1, dna: $scope.creatures[0].ai.extractDNA()});
            $scope.bestPerGeneration = $scope.bestPerGeneration.sort(function(a, b) { return b.score - a.score; });
            let bestName = $scope.bestPerGeneration[0].name;
            let bestInGeneration = false;

            mergeCreature(0, 1);
            mergeCreature(2, 3);

            for(let i = 0;  i < l; ++i)
            {
                if($scope.creatures[i].name === bestName)
                    bestInGeneration = true;
                $scope.creatures[i].x = random(50, 450);
                $scope.creatures[i].y = random(50, 200);
                $scope.creatures[i].angle = random(0, 360);
                $scope.creatures[i].life = 500;
                $scope.creatures[i].score = 0;

                let dna = $scope.creatures[i].ai.extractDNA();
                dna.mutate(.02, 2);
                createCreature(dna, $scope.creatures[i].name);
            }
            if(!bestInGeneration)
                createCreature($scope.bestPerGeneration[0].dna, bestName)
        }

        $scope.init = function()
        {
            resetFoods();
            fillGeneration();

            $scope.foodTimer = setInterval(addFood, 1000);
            $scope.updateTimer = setInterval(updateGame, 0);
        };

        $scope.endGeneration = function()
        {
            ++$scope.generation;
            resetFoods();

            $scope.creatures.sort(function(a, b) { return b.score - a.score; });
            $scope.creatures.splice(5);

            resetGeneration();

            fillGeneration();
        };

        function toRadians(angle)
        {
            return angle * (Math.PI / 180);
        }

        function random(min, max)
        {
            return Math.floor(Math.random() * (max - min + 1) + min);
        }

        function rotatedLineX(x, angle, size)
        {
            return x - Math.sin(-toRadians(angle)) * size;
        }
        function rotatedLineY(y, angle, size)
        {
            return y - Math.cos(-toRadians(angle)) * size;
        }

        function findIntersections(food, creature, sensor)
        {
            const cx = food.x;
            const cy = food.y;
            const radius = food.size;

            const point1 = {}, point2 = {};
            point1.X = creature.x;
            point1.Y = creature.y;
            point2.X = sensor.x;
            point2.Y = sensor.y;

            let dx, dy, A, B, C, det, t, t2;

            dx = point2.X - point1.X;
            dy = point2.Y - point1.Y;

            A = dx * dx + dy * dy;
            B = 2 * (dx * (point1.X - cx) + dy * (point1.Y - cy));
            C = (point1.X - cx) * (point1.X - cx) + (point1.Y - cy) * (point1.Y - cy) - radius * radius;

            det = B * B - 4 * A * C;
            if((A <= 0.0000001) || (det < 0))
            {
                return [];
            }
            else if(det === 0)
            {
                // One solution.
                t = -B / (2 * A);
                return [{ x: point1.X + t * dx, y: point1.Y + t * dy}];
            }
            else
            {
                // Two solutions.
                t = ((-B + Math.sqrt(det)) / (2 * A));
                t2 = ((-B - Math.sqrt(det)) / (2 * A));
                return [{ x: point1.X + t * dx,  y: point1.Y + t * dy },
                        { x: point1.X + t2 * dx, y: point1.Y + t2 * dy }];
            }
        }

    }

})();