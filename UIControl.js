/**
 * Created by septimus on 14/04/17.
 */

(function()
{
    angular.module('AppNeuralNet').controller('UIControlController', UIControlController);

    UIControlController.$inject = ['$scope', 'NeuralNet'];

    function UIControlController($scope, NeuralNet)
    {
        $scope._layers = [3, 2, 3, 5, 7, 1];
        $scope.functions = ['sigmoid', 'sigmoid', 'sigmoid', 'sigmoid', 'sigmoid', 'linear'];

        $scope.layers =
        [
            { function: 'sigmoid', neuronsCount: 9 },
            { function: 'sigmoid', neuronsCount: 26 },
            { function: 'sigmoid', neuronsCount: 16 },
            { function: 'sigmoid', neuronsCount: 4 }
            /*{ function: 'sigmoid', neuronsCount: 5 },
            { function: 'sigmoid', neuronsCount: 6 }*/
        ];

        $scope.init = function()
        {
            $scope.neuralnet = NeuralNet.createNewNeuralNet($scope.layers);
            console.log($scope.neuralnet);
        };

        $scope.addLayer = function()
        {
            $scope.layers.push({ function: 'sigmoid', neuronsCount: 25 });
        };

        $scope.removeLayer = function(id)
        {
            $scope.layers.splice(id, 1);
        };
        $scope.step = function()
        {
            NeuralNet.step();
        };
    }

})();