/**
 * Created by septimus on 11/03/17.
 */

angular.module('AppNeuralNet', []);

angular.module('AppNeuralNet').filter('abs', function ()
{
    return function(val)
    {
        return Math.abs(val);
    }
});

angular.module('AppNeuralNet').filter('round', function ()
{
    return function(val)
    {
        return val.toFixed(2);
    }
});