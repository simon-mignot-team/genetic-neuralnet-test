/**
 * Created by septimus on 14/04/17.
 */
(function()
{
    'use strict';

/**
    *
    * neuralNet =
    * {
    *     layers: [ Layer ]
    * };
    *
    * Layer =
    * {
    *     name: String,
    *     neurons: [ Neuron ]
    * };
    *
    * Neuron =
    * {
    *     name: String,
    *     activationFunction: function,
    *     summedValue: Integer,
    *     processedValue: Integer,
    *     weights: [ Integer ]
    * };
    *
**/

    class DNA
    {
        constructor(genes)
        {
            this.genes = genes;
        }

        mutate(mutatedGenes, mutatedMaxValue, absolute) // absolute = true -> mutate X genes instead of % genes
        {
            let mutatedGenesCount = mutatedGenes;
            if(!absolute)
                mutatedGenesCount = this.genes.length * mutatedGenes;
            mutatedGenesCount |= 0; // truncate
            console.log('Genes to mutate : ' + mutatedGenesCount + ' of ' + this.genes.length);
            let genesIDs = DNA.noDuplicateRandom(mutatedGenesCount, this.genes.length);
            for(let i = 0; i < mutatedGenesCount; ++i)
                this.mutateGene(genesIDs[i], mutatedMaxValue);
        }

        mutateGene(geneID, maxValue)
        {
            let a = this.genes[geneID];
            this.genes[geneID] += (Math.random() % (maxValue * 2)) - maxValue;
            //console.log('mutate ' + geneID + ' : ' + a + ' -> ' + this.genes[geneID]);
        }

        merge(dnaToMerge, genesToMerge, absolute)
        {
            if(this.genes.length !== dnaToMerge.genes.length)
                return 'bad dna';
            let toMergeCount = genesToMerge;
            if(!absolute)
                toMergeCount = this.genes.length * genesToMerge;
            toMergeCount |= 0; // truncate
            console.log('Genes to merge : ' + toMergeCount + ' of ' + this.genes.length);
            let genesIDs = DNA.noDuplicateRandom(toMergeCount, this.genes.length);
            for(let i = 0; i < toMergeCount; ++i)
            {
                let a = this.genes[genesIDs[i]];
                this.genes[genesIDs[i]] = dnaToMerge.genes[genesIDs[i]];
                //console.log('merge ' + genesIDs[i] + ' : ' + a + ' -> ' + this.genes[genesIDs[i]]);
            }
        }

        getGene(id)
        {
            return this.genes[id];
        }

        static noDuplicateRandom(count, max)
        {
            let result = [];
            let arr = [];
            for(let i = 0; i < max; ++i)
                arr.push(i);
            for(let i = 0; i < count; ++i)
            {
                let id = (Math.random() * arr.length) | 0;
                result.push(arr[id]);
                arr.splice(id, 1);
            }
            return result;
        }

    }

    class NeuralNet
    {
        constructor(layers)
        {
            this.config = { layers: layers, maxNeurons: NeuralNet.maxNeurons(layers) };
            this.layers = [];
            this.createLayers(layers);

        }

        extractDNA()
        {
            let dna = [];
            for(let layerID = 0; layerID < this.layers.length; ++layerID)
                for(let neuronID = 0; neuronID < this.layers[layerID].neurons.length; ++neuronID)
                    for(let weightID = 0; weightID < this.layers[layerID].neurons[neuronID].weights.length; ++weightID)
                        dna.push(this.layers[layerID].neurons[neuronID].weights[weightID]);
            return new DNA(dna);
        }
        implantDNA(dna)
        {
            let geneID = 0;
            for(let layerID = 1; layerID < this.layers.length; ++layerID)
                for(let neuronID = 0; neuronID < this.layers[layerID].neurons.length; ++neuronID)
                    for(let weightID = 0; weightID < this.layers[layerID].neurons[neuronID].weights.length; ++weightID, ++geneID)
                        this.layers[layerID].neurons[neuronID].weights[weightID] = dna.getGene(geneID);
        }

        activate()
        {
            for(let layerID = 0; layerID < this.layers.length; ++layerID)
                for(let neuronID = 0; neuronID < this.layers[layerID].neurons.length; ++neuronID)
                    this.activateNeuron(neuronID, layerID);
        }
        activateConnections(neuronID, layerID)
        {
            this.layers[layerID].neurons[neuronID].summedValue = 0;
            for(let i = 0; i < this.layers[layerID - 1].neurons.length; ++i)
            {
                const previousLayerProcessedValue = this.layers[layerID - 1].neurons[i].processedValue;
                const currentWeight               = this.layers[layerID - 1].neurons[i].weights[neuronID];
                this.layers[layerID].neurons[neuronID].summedValue += previousLayerProcessedValue * currentWeight;
            }
        }
        activateNeuron(neuronID, layerID)
        {
            if(layerID > 0)
                this.activateConnections(neuronID, layerID);
            const f = this.layers[layerID].neurons[neuronID].activationFunction === 'sigmoid' ? NeuralNet.fn_sigmoid : NeuralNet.fn_linear;
            this.layers[layerID].neurons[neuronID].processedValue = f(this.layers[layerID].neurons[neuronID].summedValue);
        }

        setInputs(inputs)
        {
            let inputsCount = this.layers[0].neurons.length;
            if(inputs.length !== inputsCount)
            {
                console.log("bad inputs : (net/input) " + inputsCount + " / " + inputs.length);
                return;
            }
            for(let i = 0; i < inputsCount; ++i)
                this.layers[0].neurons[i].summedValue = inputs[i];
        }
        getOutputs()
        {
            const lastLayerID = this.layers.length - 1;
            const neuronsCount = this.layers[lastLayerID].neurons.length;
            let outputs = [];
            for(let i = 0; i < neuronsCount; ++i)
                outputs.push(this.layers[lastLayerID].neurons[i].processedValue);
            return outputs;
        }


        createLayers(layers)
        {
            for(let i = 0; i < layers.length; ++i)
            {
                const connectionsCount = i < layers.length - 1 ? layers[i + 1].neuronsCount : 0;
                const name = NeuralNet.getLayerName(i, layers.length - 1) + i;
                const l = NeuralNet.createLayer(layers[i].neuronsCount, connectionsCount, layers[i].function, name);
                this.layers.push(l);
            }
            return this.layers;
        }
        static createLayer(neuronCount, neuronCountInNextLayer, activationFunction, layerName)
        {
            const result = {name: layerName, neurons: []};
            for(let i = 0; i < neuronCount; ++i)
            {
                const n = {};
                n.name = layerName + '-n' + i;
                n.activationFunction = activationFunction;
                n.summedValue = layerName === 'in0' ? Math.random() * 2 - 1 : 0;
                n.processedValue = 0;
                n.weights = NeuralNet.generateRandomWeights(neuronCountInNextLayer);
                result.neurons.push(n);
            }
            return result;
        }
        static getLayerName(layerID, lastID)
        {
            let name = 'l';
            if(layerID === 0)
                name = 'in';
            else if(layerID === lastID)
                name = 'out';
            return name;
        }
        static generateRandomWeights(count)
        {
            const result = [];
            for(let i = 0; i < count; ++i)
                result.push(Math.random() * 2 - 1);
            return result;
        }
        static maxNeurons(layers)
        {
            const array = [];
            for(let i = 0; i < layers.length; ++i)
            {
                array.push(layers[i].neuronsCount);
            }
            return Math.max.apply(null, array);
        }

        static fn_sigmoid(x)
        {
            return 1/(1+Math.pow(Math.E, -x));
        }
        static fn_linear(x)
        {
            return x;
        }
    }

    angular.module('AppNeuralNet').factory('NeuralNet', FactoryNeuralNet);

    FactoryNeuralNet.$inject = ['$rootScope'];

    function FactoryNeuralNet($rootScope)
    {
        /*let neuralnet =
        {
            layers: [
                {
                    name: '',
                    neurons: [
                        {
                            name: '',
                            activationFunction: '',
                            summedValue: 0,
                            processedValue: 0,
                            weights: []
                        }
                    ]
                }
            ]
        };*/

        let neuralNets = [];
        function createNewNeuralNet(layers)
        {
            neuralNets.push(new NeuralNet(layers));
            $rootScope.$broadcast('NewNeuralNet');
            console.log(layers);
            return neuralNets[neuralNets.length - 1];
        }
        function getNeuralNet(id)
        {
            if(typeof(id) === 'undefined')
                id = neuralNets.length - 1;
            return neuralNets[id];
        }




        /*function step()
        {
            for(let layerID = 0; layerID < neuralnet.layers.length; ++layerID)
                for(let neuronID = 0; neuronID < neuralnet.layers[layerID].neurons.length; ++neuronID)
                    activateNeuron(neuronID, layerID);

            neuralnet.layers[0].neurons[0].summedValue += 0.01;
            $rootScope.$broadcast('NewNeuralNet');
        }*/

        return {
            createNewNeuralNet: createNewNeuralNet,
            getNeuralNet: getNeuralNet
            //step: step
        };
    }

})();