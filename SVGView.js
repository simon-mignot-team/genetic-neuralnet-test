/**
 * Created by septimus on 09/04/17.
 */

(function()
{

    angular.module('AppNeuralNet').controller('SVGViewController', SVGViewController);

    SVGViewController.$inject = ['$scope', 'NeuralNet'];

    function SVGViewController($scope, NeuralNet)
    {

        $scope.$on('NewNeuralNet', function() { $scope.getNewNeuralNet(); });

        $scope.config =
        {
            padding:        100,
            neuronSize:     20,
            layerDistance:  50,
            neuronDistance: 10
        };

        $scope.container =
        {
            viewBox: {}
        };

        $scope.content =
        {

        };

        function calculateContent()
        {
            $scope.content.originX = $scope.config.padding + $scope.config.neuronSize / 2;
            $scope.content.originY = $scope.config.padding + $scope.config.neuronSize / 2;
            $scope.content.layerDistance = $scope.config.layerDistance + $scope.config.neuronSize * 2;
            $scope.content.neuronDistance = $scope.config.neuronDistance + $scope.config.neuronSize * 2;
        }

        $scope.init = function()
        {
            calculateContent();
        };
        $scope.getNewNeuralNet = function()
        {
            console.log("SVG new neural net");
            $scope.neuralnet = NeuralNet.getNeuralNet();
            const l = $scope.neuralnet.config.layers.length;
            const n = $scope.neuralnet.config.maxNeurons;
            $scope.refreshViewBox(l, n);
        };
        $scope.refreshViewBox = function(l, n)
        {
            $scope.container.viewBox.x = 0;
            $scope.container.viewBox.y = 0;
            const w = l * $scope.content.layerDistance + $scope.config.padding * 2;
            const h = n * $scope.content.neuronDistance + $scope.config.padding * 2;

            if(w / 2 > h)
            {
                $scope.container.viewBox.width = w;
                $scope.container.viewBox.height = w / 2;
            }
            else
            {
                $scope.container.viewBox.width = h * 2;
                $scope.container.viewBox.height = h;
            }
            console.log($scope.container)
        };
        $scope.getNeuronCenterX = function(layerID)
        {
            return $scope.content.originX + $scope.config.neuronSize / 2   // neuron center + padding
                   + layerID * $scope.content.layerDistance;      // select layer
        };
        $scope.getNeuronCenterY = function(neuronID, layerID)
        {
            let centering = 0;
            if(typeof(layerID) === 'number')
            {
                const maxNeurons = $scope.neuralnet.config.maxNeurons;
                const neuronsCount = $scope.neuralnet.layers[layerID].neurons.length;
                const neuronDistance = $scope.content.neuronDistance;
                //centering = (neuronDistance * (maxNeurons - neuronsCount) / maxNeurons);
                centering = neuronDistance * ((maxNeurons - neuronsCount) / 2);
            }
            return  $scope.content.originY + $scope.config.neuronSize / 2
                    + neuronID * $scope.content.neuronDistance
                    + centering;
        };
        $scope.getColor = function(weight)
        {
            const color = weight > 0 ? '0, 0, 255' : '255, 0, 0';
            const alpha = (Math.abs(weight) / 1.9 + .1);
            return 'rgba(' + color + ', ' + alpha + ')';
        };
    }

})();